FROM docker.io/google/cloud-sdk:alpine

ARG TERRAFORM_VERSION

RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
  && unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
  && mv terraform /usr/bin/terraform \
  && rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip
