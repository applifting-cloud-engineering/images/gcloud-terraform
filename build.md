
# How to build Image with custom version

There is also an option to add a custom version manually as well as a tag of the image.

To build an image with versions of your choice run the [pipeline manually](https://gitlab.com/applifting-cloud-engineering/images/gcloud-terraform/-/pipelines/new) and provide this variable:
- `TF_VERSION` - you can find Terraform releases [here](https://github.com/hashicorp/terraform/releases). If not specified the latest version is used.
