# Gcloud + Terraform Docker Image

This repo serves the purpose of creating Docker images to be used in CI/CD pipelines.
The CI/CD job in this project is scheduled to run weekly and build a new Docker image with the latest tool's version while tagging the image `latest`.
The image is also tagged with `{TERRAFORM_VERSION}`.

### You can find all the previous versions in the [Container Registry](https://gitlab.com/applifting-cloud-engineering/images/gcloud-terraform/container_registry/3365239).
